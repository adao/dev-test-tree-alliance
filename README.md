# Base Docker Template

  Use this to set up a local environment for your Wordpress dev project. This is the Docker configuration we would use for our own projects, so having parity helps us to set up your project on our side!

  Stuck? Don't worry, just get back in touch with us, or carry on using whatever stack you are most familiar with.

# Prerequisites
  
  1. Bash (if you're using macOS or Linux, you should have this already available. If using Windows, you will need to install Windows Subsystem for Linux: https://docs.microsoft.com/en-us/windows/wsl/install-win10 )


  2. Docker Desktop (tested up to version 4.0)


# Steps

  1. Run `bash start` in your command line at this directory level


  2. To access your new WordPress site, visit <http://localhost:4444>
  
  >  Trouble loading the site? Check that the ports defined in docker-compose.yml are the same as above.


  3. To export your database, run `bash export /tmp/dump.sql`. You can change the path to anything you like (`tmp` is used as an example here)
  
  
 Need to set up a fresh container with an existing database? Drop your database file into the db folder and run `bash start`.
